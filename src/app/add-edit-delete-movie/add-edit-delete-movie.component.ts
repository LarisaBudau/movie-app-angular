import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {MovieModel} from "../model/movie.model";
import {MovieService} from "../movie.service";

@Component({
  selector: 'app-add-edit-delete-movie',
  templateUrl: './add-edit-delete-movie.component.html',
  styleUrls: ['./add-edit-delete-movie.component.css']
})
export class AddEditDeleteMovieComponent{
  @Input() movie: MovieModel;

  @Output() changeData: EventEmitter<any> = new EventEmitter<any>();

  constructor(private movieService: MovieService) {
    this.movie = {
      id: "",
      title: "",
      description: "",
      year: "",
      director: "",
    };
  }

  onSaveMovie(): void {
    console.log(this.movie);
    let movieToSave = {
      id: this.movie.id,
      title: this.movie.title,
      description: this.movie.description,
      year: this.movie.year,
      director: this.movie.director
    };
    this.movieService.addMovie(movieToSave).subscribe((response: any) => {
      console.log(response);
      this.changeData.emit();
    });
    this.onClear();
  }

  onClear(): void {
    this.movie = {
      id: "",
      title: "",
      description: "",
      year: "",
      director: "",
    };
  }

  onDeleteMovie(): void {
    this.movieService.deleteMovie(this.movie).subscribe((response: any) => {
      console.log(response);
      this.changeData.emit();
      this.onClear();
    });
  }

  onEditMovie(): void {
    this.movieService.updateMovie(this.movie).subscribe((response: any) => {
      console.log(response);
      this.changeData.emit();
      this.onClear();
    });
  }
}
