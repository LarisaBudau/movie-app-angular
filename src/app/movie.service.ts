import { Injectable } from '@angular/core';
import {MovieModel} from "./model/movie.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private movies: Array<MovieModel>=[];
  apiUrl: string = "https://api.codebyte-software.com:2323/api"
  constructor(private httpClient: HttpClient) {

  }
  public getMovies(): Array<MovieModel>{
    return this.movies;
  }

  public addMovie(movie: MovieModel): any{
    return this.httpClient.post(`${this.apiUrl}/movie`, movie);
    // this.movies.push(movie);
    // console.log(this.movies);
  }

  public loadMovies(): any{
    return this.httpClient.get(`${this.apiUrl}/movie`);
  }

  public updateMovie(movie: MovieModel): any{
    return this.httpClient.patch(`${this.apiUrl}/movie/${movie.id}`, movie);
  }

  public deleteMovie(movie: MovieModel): any{
    return this.httpClient.delete(`${this.apiUrl}/movie/${movie.id}`);
  }
}
