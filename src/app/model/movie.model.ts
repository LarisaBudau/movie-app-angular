export interface MovieModel {
  id: any;
  title: string;
  description: string;
  year: string;
  director: string;
}
