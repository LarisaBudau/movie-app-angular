import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {

  viewType: string = "login";
  email: string = "";
  password = "";
  name: string = "";
  retypePassword: string = "";

  constructor(private router: Router, private authService: AuthService) {

  }

  onClearEmail(): void {
    console.log("before:" + this.email);
    this.email = "";
    console.log("after:" + this.email);
  }

  onRegisterClick(): void {
    this.viewType = "register";
  }

  onLoginClick(): void {
    this.viewType = "login";
  }

  onSubmitLogin(): void {
    // this.router.navigate(["/","dashboard"])
    this.authService.login(this.email, this.password).subscribe((response: any) => {
      console.log(response);
      this.router.navigate(["/", "dashboard"])
    });
  }

  onSubmitRegister(): void{
    if (this.password != this.retypePassword){
      alert("Parolele sunt diferite!");
      return;
    }
    this.authService.register(this.name, this.email, this.password, this.retypePassword).subscribe((response: any) => {
      console.log(response);
      this.viewType = "login";
    });
  }
}
