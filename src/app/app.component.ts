import { Component } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MovieManager';
}

//logica de functionare a componentei
//va spunem ce va face un button cand dam clik
