import {Component, OnInit} from '@angular/core';
import {MovieModel} from "../model/movie.model";
import {MovieService} from "../movie.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{

  movieList: Array<MovieModel> = [];

  selectedMovie: MovieModel;

  constructor(private movieService: MovieService) {


    this.selectedMovie = {
      id: "",
      title: "",
      description: "",
      year: "",
      director: ""
    };
  }

  ngOnInit() {
    this.loadMovies();
  }
  loadMovies(){
    this.movieService.loadMovies().subscribe((response: any)=>{
      this.movieList = response.data;
      console.log(response.data);

    });
    console.log("Datele sunt actualizate.");
  }


  onSelectMovie(movie: MovieModel): void {
    console.log(movie);
    this.selectedMovie = {
      id: movie.id,
      title: movie.title,
      description: movie.description,
      year: movie.year,
      director: movie.director
    };
  }

}
