import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl: string = "https://api.codebyte-software.com:2323/api"

  constructor(private httpClient: HttpClient) {

  }

  login(email: string, password: string): any {
    let payload = {
      "email": email,
      "password": password
    };
    console.log(`${this.apiUrl}/auth/login`);
    return this.httpClient.post(`${this.apiUrl}/auth/login`, payload);
  }

  register(name: string, email: string, password: string, retypePassword: string): any {
    let payload = {
      "name": name,
      "email": email,
      "password": password,
      "retypePassword": retypePassword
    };
    console.log(`${this.apiUrl}/auth/register`);
    return this.httpClient.post(`${this.apiUrl}/auth/register`, payload);
  }
}
